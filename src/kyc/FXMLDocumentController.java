/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kyc;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyEvent;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author abdulmumun
 */
public class FXMLDocumentController  {
    
     @FXML
    private TextField asset_ira_textfield;

   
    @FXML
    private TextField asset_cash_textfield;

    @FXML
    private TextField asset_noteRecievable_textfield;

    @FXML
    private TextField asset_insurance_textfield;

    @FXML
    private TextField assset_stockBond_textfield;

    @FXML
    private TextField asset_automobile_textfield;

    @FXML
    private TextField asset_otherPersonalProperty_textfield;

    @FXML
    private TextField asset_otherAssets_textfield;

    @FXML
    private TextField sourceOfIncome_salary_textfield;

    @FXML
    private TextField sourceOfIncome_netInvestment_textfield;

    @FXML
    private TextField sourceOfIncome_realEstate_textfield;

    @FXML
    private TextField sourceOfIncome_otherIncome_textfield;

    @FXML
    private TextField liabilities_accountPayable_textfield;

    @FXML
    private TextField liabilities_notePayable_textfield;

    @FXML
    private TextField liabilities_installmentAccount_textfield;

    @FXML
    private TextField liabilities_loanOnLifeInsurance_textfield;

    @FXML
    private TextField liabilities_mortgageOnRealEstate_textfield;

    @FXML
    private TextField liabilities_unpaidTaxes_textfield;

    @FXML
    private TextField liabilities_otherLiabilities_textfield;

    @FXML
    private TextField contingent_endorser_textfield;

    @FXML
    private TextField contingent_legalClaim_textfield;

    @FXML
    private TextField contingent_provisionForTaxes_textfield;

    @FXML
    private TextField contingent_otherSpecialDebt_textfield;
    
    @FXML
    private Label contingent_total;
    
    double sum =0;
    double num1, num2 = 0;
    
    
    
     @FXML
    private void ihandleButtonAction(KeyEvent event) {
        sum=0.0;
        num1=Double.parseDouble(contingent_provisionForTaxes_textfield.getText());
        
        sum=sum+num1 + num2;
        

       contingent_total.setText(String.valueOf(sum));
        sum=0.0;
        num2=Double.parseDouble(contingent_otherSpecialDebt_textfield.getText());
        sum=sum+num1 + num2;
        

       contingent_total.setText(String.valueOf(sum));
       
        
        
    }
    
       @FXML
    private void yhandleButtonAction(KeyEvent event) {
       sum=0.0;
        num2=Double.parseDouble(contingent_otherSpecialDebt_textfield.getText());

       contingent_total.setText(String.valueOf(sum+=num2));
        
    }
    
       @FXML
    void handleButtonAction(KeyEvent event) {
        
        System.out.println(String.valueOf(event.getText()));

    }
    
    @FXML
    public void initialize (){
    
   contingent_otherSpecialDebt_textfield.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
        if (!newValue.matches("\\d{0,9}([\\.]\\d{0,2})?")) {
             contingent_otherSpecialDebt_textfield.setText(oldValue);
            
            
        }
    });
    
    contingent_provisionForTaxes_textfield.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
        if (!newValue.matches("\\d{0,9}([\\.]\\d{0,2})?")) {
             contingent_provisionForTaxes_textfield.setText(oldValue);
             
        }
    });
    
    }
      
   /*@FXML
   EventHandler<javafx.scene.input.KeyEvent> eventHandler = 
   new EventHandler<javafx.scene.input.KeyEvent>() { 
   
 @FXML
   public void handle(javafx.scene.input.KeyEvent e) { 
      //System.out.println(e); 
              
   } */
    
    
}; 
   
  
     
    
    
    
    
    /*   UnaryOperator<TextFormatter.Change> filter = new UnaryOperator<TextFormatter.Change>() {

            @Override
            public TextFormatter.Change apply(TextFormatter.Change t) {

                if (t.isReplaced()) 
                    if(t.getText().matches("[^0-9]"))
                        t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
                

                if (t.isAdded()) {
                    if (t.getControlText().contains(".")) {
                        if (t.getText().matches("[^0-9]")) {
                            t.setText("");
                        }
                    } else if (t.getText().matches("[^0-9.]")) {
                        t.setText("");
                    }
                }

                return t;
            }
        };

        
       contingent_provisionForTaxes_textfield.setTextFormatter(new TextFormatter<>(filter));*/

